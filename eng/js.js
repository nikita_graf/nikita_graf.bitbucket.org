function ajax1() {
  var e = $("#form-1")
    .serialize();
  $.ajax({
    type: "POST",
    url: "./js/send.php",
    data: e,
    success: function (e) {
      $("#modal-success div")
        .html(e), $("#modal-success")
        .addClass("show1"), setTimeout(function () {
        $("#modal-success")
          .addClass("show2")
      }, 1), $("#modal-bg")
        .addClass("show1"), setTimeout(function () {
        $("#modal-bg")
          .addClass("show2")
      }, 1), $("body")
        .css({
          overflow: "hidden",
          "padding-right": swidth
        }), $(".header")
        .css({
          "margin-right": swidth
        }), setTimeout(function () {
        $("#modal-bg")
          .removeClass("show2"), setTimeout(function () {
          $("#modal-bg")
            .removeClass("show1")
        }, 300), $("#modal-success")
          .removeClass("show2"), setTimeout(function () {
          $("#modal-success")
            .removeClass("show1")
        }, 1), setTimeout(function () {
          $("body")
            .css({
              overflow: "auto",
              "padding-right": 0
            })
        }, 500)
      }, 3700)
    },
    error: function (e, o) {
      alert("Возникла ошибка!")
    }
  })
}

function ajax2() {
  var e = $("#form-2")
    .serialize();
  $.ajax({
    type: "POST",
    url: "./js/send.php",
    data: e,
    success: function (e) {
      $("#modal-success div")
        .html(e), $("#modal-success")
        .addClass("show1"), setTimeout(function () {
        $("#modal-success")
          .addClass("show2")
      }, 1), $("#modal-bg")
        .addClass("show1"), setTimeout(function () {
        $("#modal-bg")
          .addClass("show2")
      }, 1), $("body")
        .css({
          overflow: "hidden",
          "padding-right": swidth
        }), $(".header")
        .css({
          "margin-right": swidth
        }), setTimeout(function () {
        $("#modal-bg")
          .removeClass("show2"), setTimeout(function () {
          $("#modal-bg")
            .removeClass("show1")
        }, 300), $("#modal-success")
          .removeClass("show2"), setTimeout(function () {
          $("#modal-success")
            .removeClass("show1")
        }, 1), setTimeout(function () {
          $("body")
            .css({
              overflow: "auto",
              "padding-right": 0
            })
        }, 500)
      }, 3700)
    },
    error: function (e, o) {
      alert("Возникла ошибка!")
    }
  })
}

$(function () {
  $('.form-10').submit(function (e) {
    var $form = $(this);
    var msg = $form.serialize();
    var $modal = $form.parents('.modal');

    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "./js/send.php",
      data: msg,
      success: function (data) {
        $modal.removeClass('show2');
        $("#modal-success div").html(data);
        $("#modal-success").addClass("show1");

        setTimeout(function () {
          $modal.removeClass('show1')
        }, 300);
        setTimeout(function () {
          $("#modal-success").addClass("show2")
        }, 1);

        setTimeout(function () {
          $('#modal-bg').removeClass('show2');
          $("#modal-success").removeClass("show2");

          setTimeout(function () {
            $('#modal-bg').removeClass('show1')
          }, 300);
          setTimeout(function () {
            $("#modal-success").removeClass("show1")
          }, 1);
          setTimeout(function () {
            $('body').css({'overflow': 'auto', 'padding-right': 0});
          }, 500);
        }, 3700);

        yaCounter31709306.reachGoal('submit');
      },
      error: function (xhr, str) {
        alert("Error!")
      }
    });
  })
});

function parseGET(e) {
  if (e && "" != e || (e = decodeURI(document.location.search)), e.indexOf("?") < 0) return Array();
  e = e.split("?"), e = e[1];
  var o = [],
    t = [],
    s = [];
  for (-1 != e.indexOf("#") && (e = e.substr(0, e.indexOf("#"))), e.indexOf("&") > -1 ? t = e.split("&") : t[0] = e, r = 0; r < t.length; r++)
    for (z = 0; z < namekey.length; z++) t[r].indexOf(namekey[z] + "=") > -1 && t[r].indexOf("=") > -1 && (s = t[r].split("="), o[s[0]] = s[1]);
  return o
}

function addUtm() {
  var e = parseGET();
  for (z = 0; z < namekey.length; z++) e[namekey[z]] && ($("#form-1")
    .append('<input name="' + namekey[z] + '" type="hidden" value="' + e[namekey[z]] + '">'), $("#form-2")
    .append('<input name="' + namekey[z] + '" type="hidden" value="' + e[namekey[z]] + '">'))
}
document.body.style.overflow = "hidden";
var swidth = document.body.clientWidth;
document.body.style.overflow = "scroll",
swidth -= document.body.clientWidth, swidth || (swidth = document.body.offsetWidth - document.body.clientWidth),
document.body.style.overflow = "";

jQuery.fn.notExists = function () {
  return 0 == $(this)
      .length
};

$(document)
  .ready(function () {
    $("#send-1")
      .validation($("#tel-1")
        .validate({
          test: "blank",
          invalid: function () {
            $(".error")
              .remove(), $(this)
              .nextAll(".error")
              .notExists() && ($(this)
              .after('<div class="error">Enter the phone</div>'), $(this)
              .nextAll(".error")
              .delay(1500)
              .fadeOut("slow"), setTimeout(function () {
              $("#tel-1")
                .next(".error")
                .remove()
            }, 2100))
          },
          valid: function () {
            $(this)
              .nextAll(".error")
              .remove()
          }
        }));

    $("#send-2")
      .on("click", function () {
        if (null == $("#tel-2")
            .val() || "" == $("#tel-2")
            .val()) {
          if (null == $("#mail-2")
              .val() || "" == $("#mail-2")
              .val()) return $(".error")
            .remove(), $("#send-2")
            .after('<div class="error">Enter the phone or e-mail</div>'), $("#send-2")
            .nextAll(".error")
            .delay(1500)
            .fadeOut("slow"), setTimeout(function () {
            $("#send-2")
              .next(".error")
              .remove()
          }, 2100), !1;
          $(".error")
            .remove()
        } else $(".error")
          .remove()
      });

    $(".send-10").on('click',function() {
      var $self = $(this);
      var $form = $(this).parents('form');
      var $phone = $form.find('.tel-10');
      var $email = $form.find('.mail-10');

      if(($phone.val()==null) || ($phone.val()=='')) {
        if(($email.val()==null)||($email.val()=='')) {
          $form.find(".error").remove();
          $self.after('<div class="error">Phone and e-mail are required</div>');
          $self.nextAll(".error").delay(1500).fadeOut("slow");
          setTimeout(function(){
            $self.next(".error").remove();
          },2100);
          return false;
        } else {
          $form.find(".error").remove();
        }
      }else{
        $form.find(".error").remove();
      }
    });
  });
var namekey = ["utm_source", "utm_medium", "utm_campaign", "type", "source", "added", "block", "pos", "key", "campaign", "ad", "phrase", "utm_term", "utm_content", "network", "placement", "keyword"];
! function (e) {
  (jQuery.browser = jQuery.browser || {})
    .mobile = /(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(e) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0, 4))
}(navigator.userAgent || navigator.vendor || window.opera), $(document)
  .ready(function () {
    $(".promo")
      .outerHeight($(window)
        .height()), $(window)
      .resize(function () {
        $(".promo")
          .outerHeight($(window)
            .height())
      }), jQuery.browser.mobile || $(window)
      .width() > 768 && $(function () {
      var e = $(".parallax"),
        o = -($(window)
          .scrollTop() / e.data("speed")),
        t = "50% " + o + "px";
      e.css({
        "background-attachment": "fixed",
        "background-position": t
      });
      var s = $(".parallax1"),
        a = -($(window)
            .scrollTop() / s.data("speed")) + $(".parallax1")
            .offset()
            .top / s.data("speed"),
        i = "50% " + a + "px";
      s.css({
        "background-attachment": "fixed",
        "background-position": i
      });
      var d = $(".parallax2"),
        n = -($(window)
            .scrollTop() / d.data("speed")) + $(".parallax2")
            .offset()
            .top / d.data("speed"),
        r = "50% " + n + "px";
      d.css({
        "background-attachment": "fixed",
        "background-position": r
      });
      var l = $(".parallax3"),
        c = -($(window)
            .scrollTop() / l.data("speed")) + $(".parallax3")
            .offset()
            .top / l.data("speed"),
        m = "50% " + c + "px";
      l.css({
        "background-attachment": "fixed",
        "background-position": m
      }), $(window)
        .scroll(function () {
          var o = -($(window)
            .scrollTop() / e.data("speed")),
            t = "50% " + o + "px";
          e.css({
            backgroundPosition: t
          });
          var a = -($(window)
              .scrollTop() / s.data("speed")) + s.offset()
                .top / s.data("speed"),
            i = "50% " + a + "px";
          s.css({
            backgroundPosition: i
          });
          var n = -($(window)
              .scrollTop() / d.data("speed")) + d.offset()
                .top / d.data("speed"),
            r = "50% " + n + "px";
          d.css({
            backgroundPosition: r
          });
          var c = -($(window)
              .scrollTop() / l.data("speed")) + l.offset()
                .top / l.data("speed"),
            m = "50% " + c + "px";
          l.css({
            backgroundPosition: m
          });
          var u = $(window)
            .scrollTop();
          u > $("#service")
            .offset()
            .top && u < $("#service")
            .offset()
            .top + $("#service")
            .height() - 300 ? $("#service")
            .addClass("fixhead") : $("#service")
            .removeClass("fixhead")
        })
    });

    $(function () {
      $("#altvisa_nform")
        .val($(".altvisa_step_item-active .altvisa_step_link")
          .attr("data-visa")), $(".altvisa_step_link")
        .on("click", function () {
          return $(".altvisa_step_item")
            .removeClass("altvisa_step_item-active"), $(this)
            .parents(".altvisa_step_item")
            .addClass("altvisa_step_item-active"), $("#altvisa_nform")
            .val($(this)
              .attr("data-visa")), !1
        })
    });

    !function (e) {
      e.fn.innerEqualHeights = function () {
        var o = 0,
          t = e(this);
        return t.each(function () {
          var t = e(this)
            .innerHeight();
          t > o && (o = t)
        }), t.innerHeight(o)
      }, e("[data-equal]")
        .each(function () {
          var o = e(this),
            t = o.data("equal");
          o.find(t)
            .innerEqualHeights()
        })
    }(jQuery);

    !function (e) {
      e.fn.equalHeights = function () {
        var o = 0,
          t = e(this);
        return t.each(function () {
          var t = e(this)
            .height();
          t > o && (o = t)
        }), t.height(o)
      }, e("[data-equal]")
        .each(function () {
          var o = e(this),
            t = o.data("equal");
          o.find(t)
            .equalHeights()
        })
    }(jQuery);

    $(".contact_vcard")
      .innerEqualHeights(), addUtm(), $(".order")
      .click(function () {
        return $("#modal-1")
          .addClass($(this)
            .attr("data-class")), $(".modal_header-w")
          .html($(this)
            .attr("data-header")), $("#send-10")
          .val($(this)
            .attr("data-send")), $("#nform")
          .val($(this)
            .attr("data-nform")), $("#modal-1")
          .addClass("show1"), setTimeout(function () {
          $("#modal-1")
            .addClass("show2")
        }, 1), $("#modal-bg")
          .addClass("show1"), setTimeout(function () {
          $("#modal-bg")
            .addClass("show2")
        }, 1), $("body")
          .css({
            overflow: "hidden",
            "padding-right": swidth
          }), $(".header")
          .css({
            "margin-right": swidth
          }), !1
      });


    (function() {
      var activeModal;

      // modal
      $('.order').click(function () {
        var modalSelector = $(this).attr('data-modal-target');
        var $modal = $(modalSelector);

        activeModal = $modal;
        $modal.addClass('show1');
        $('#modal-bg').addClass('show1');
        $('body').css({
          'overflow': 'hidden',
          'padding-right': swidth
        });
        $('.header').css({
          'margin-right': swidth
        });

        setTimeout(function () {
          $modal.addClass('show2');
          $('#modal-bg').addClass('show2');
        }, 1);

        return false;
      });


      $('#modal-bg, .modal_close').click(function () {
        var $modal = activeModal;

        $modal.removeClass('show2');
        $('#modal-bg').removeClass('show2');
        $('#modal-success').removeClass('show2');

        setTimeout(function () {
          $modal.removeClass('show1 type-1 type-2 type-3-1 type-3-2 type-3-3 type-3-4 type-4 type-5-1 type-5-2 type-5-3 type-5-4 type-5-5 type-5-6 type-5-7 type-5-8 type-5-9 type-6');
          $('#modal-bg').removeClass('show1');
          $('#modal-success').removeClass('show1');
          $('body').css({'overflow': 'auto', 'padding-right': 0});
          $('.header').css({'margin-right': 0})
        }, 500);

        return false;
      });

    })();

    $(function () {
      function e() {
        var e = new google.maps.LatLng(51.510594, -.137993),
          o = new google.maps.LatLng(51.513, -.137993),
          t = {
            zoom: 15,
            center: o,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: !1
          },
          s = new google.maps.Map(document.getElementById("contact_map-london"), t),
          a = new google.maps.Marker({
            position: e,
            map: s,
            title: "«UKVISAS.COM» – ПМЖ и гражданство<br>Великобритании через инвестиции"
          })
      }

      e()
    })
  });